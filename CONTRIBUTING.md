# TODO LIST

## Automatic Phase scan

**Pad Mode:**

1. Power up
2. Switch off all unnecessary analogue functionality
3. Clear the chip
4. Insert a single one into each of the pad regions
5. Adjust the receiver phase to understand where exactly we receive what we asked for
6. Verify with a different value
7. Shift a constant value through the pads to understand whether different columns react differently to the same value

**Strip Mode:**

1. Power up
2. Switch off all unnecessary analogue functionality
3. Clear the chip
4. Insert a single one
5. Set the phase so as to receive 40 as a received word on the highest output channel
6. Iterate through the columns by injecting a zero every time to see numbers progressing as 10, 04, 01 and then shifting into the next output
7. Verify that next output works, if it doesn't, investigate phase based on that output and make one phase scan per output
8. Once phase(s) has(have) thus been verified, validate with different numbers (random sequence?)

## Text Output

Need to fix output from serveral commands to make the code and the devices response more readable.

## Automatic check of digital functionality

Scan all possible input vectors (too many, be smart for subsections) to determine whether full digital functionality is there.

- Done: `runStripCheck()`
- Done: `runPadCheck()`
- Need to get a Phase scan per channel going, as data comes back corrupted every now and then
-- still need to process data to understand whether certain channels are more liable to data corruption than others
