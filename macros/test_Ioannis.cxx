#include "DecalMotherboard.h"

void printBinaryIoannis(unsigned int number, unsigned int nBits, unsigned int startBit) {
	unsigned int debugCount = 0;
	number = number << (31-startBit);
	while (debugCount < nBits) {
		if (number & (0x1 << 31)) {
			printf("1.");
		} else {
			printf("0.");
		}
		number = number << 1;
		debugCount++;
		if (debugCount > 256 || debugCount > nBits) {
			break;
		}
	}
	return;
}

void captureDataIoannis(std::vector<StreamData> &returnData, unsigned int capLength, unsigned int nChannels, bool sorted)
{
	returnData.clear();
	// Check capLength
	if (capLength < 1) {
		printf("Not capturing nothin', screw you!\n");
		return;
	}
//	printf("Capturing %d words\n", capLength);
	e->HsioFlush();
	e->ConfigureVariable(10019, 0x1000); // Disable Trigger-Data packets
	e->ConfigureVariable(10007, capLength); // Seems to request a certain length of capture data?
	Matt_ConfStreamsAll(0,0); // Disable all streams?
	Matt_ConfStream(0x8811,  0, nChannels*2,  1); // Enable 2 streams, 0 and 1, or does it?
	Matt_ConfStreamsAll(0,0);
	uint16_t opcode;
	uint16_t seqnum;
	uint16_t length;
	uint16_t *recv_data=0;
	opcode = 0xd004; // 4 for capture, 8 for ABC130 packets
	int error =  0;
	int error_prev = 0;
	for(unsigned int channel=0; channel<nChannels; channel++) {
		returnData.push_back(receiveData(capLength)); // Allready receiving Data - but we have not yet sent any triggers...
//        printf("Received Data for Channel %02d\n", returnData.back().getChannel());
	}
	if (sorted) {
		std::sort(returnData.begin(), returnData.end());
	}
	if (error!=-1) {
		if (error>0) printf("%d ERRORS!!\n",error);
//		else printf("\nNo errors\n");
	}
	return;
}

void printDataIoannisPadDecimal(std::vector<StreamData> &data, bool sorted) {
	unsigned int channelIndex = 31; //< 15*2
	bool firstPrint = true;
	while (true) {
		for (unsigned int i=0; i<data.size(); i++) {
			if (!(firstPrint)) { printf(""); firstPrint = true; }
			if (data[i].getChannel() == channelIndex) {
//				printf("%03d\t%02d\t", data.at(i).getRawData(0).getHit(1), channelIndex);
				printf("%03d\t.", data.at(i).getRawData(0).getHit(1));
//				printBinary(data.at(i).getRawData(0).getHit(1), 8, 7);
				firstPrint = false;
				break;
			}
		}
		if(channelIndex == 0) break;
		channelIndex--;
	}
	//printf("\n");
}


void printDataIoannisPad(std::vector<StreamData> &data, bool sorted) {
	unsigned int channelIndex = 31; //< 15*2
	bool firstPrint = true;
	while (true) {
		for (unsigned int i=0; i<data.size(); i++) {
			if (!(firstPrint)) { printf(""); firstPrint = true; }
			if (data[i].getChannel() == channelIndex) {
//				printf("%03d\t%02d\t", data.at(i).getRawData(0).getHit(1), channelIndex);
//				printf("%03d\t.", data.at(i).getRawData(0).getHit(1));
				printBinaryIoannis(data.at(i).getRawData(0).getHit(1), 8, 7);
				firstPrint = false;
				break;
			}
		}
		if(channelIndex == 0) break;
		channelIndex--;
	}
	//printf("\n");
}

void printDataIoannisStrip(std::vector<StreamData> &data, bool sorted) {
	unsigned int channelIndex = 31; //< 15*2
	bool firstPrint = true;
	while (true) {
		for (unsigned int i=0; i<data.size(); i++) {
			if (!(firstPrint)) { printf(""); firstPrint = true; }
			if (data[i].getChannel() == channelIndex) {
//				printf("%03d\t%02d\t", data.at(i).getRawData(0).getHit(1), channelIndex);
//				printf("%03d\t.", data.at(i).getRawData(0).getHit(1));
				printBinaryIoannis(data.at(i).getRawData(0).getHit(1), 8, 7);
				firstPrint = false;
				break;
			}
		}
		if(channelIndex == 0) break;
		channelIndex--;
	}
	//printf("\n");
}

void debugMeIoannisPad(unsigned int capLength, unsigned int nChannels, bool sorted) {
	// printf("Capturing Data on %d streams\n", nChannels);
	std::vector<StreamData> v;
	// TH2F* myHistogram = new TH2F("myName", "againMyName", 128, 0, 127, 66, 0, 65);
	int phase = 0;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPad(v, true); 
	inject_number(6);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPad(v, true); 
	inject_number(5);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPad(v, true); 
	inject_number(4);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPad(v, true); 
	for (unsigned int i=0; i<64; i++) {
		phase++;
		inject_zero();
		v.clear();
//		setManualPhase(true, phase);
		captureDataIoannis(v, capLength, nChannels);
		printf("%02d\t.", phase);
        //myHistogram->SetBinContent(10, i, 1);
		printDataIoannisPad(v, true); 
	}
}


void debugMeIoannisPadDecimal(unsigned int capLength, unsigned int nChannels, bool sorted) {
	// printf("Capturing Data on %d streams\n", nChannels);
	std::vector<StreamData> v;
	// TH2F* myHistogram = new TH2F("myName", "againMyName", 128, 0, 127, 66, 0, 65);
	int phase = 0;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPadDecimal(v, true); 
	inject_number(256);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPadDecimal(v, true); 
	inject_number(256);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPadDecimal(v, true); 
	inject_number(256);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisPadDecimal(v, true); 
	for (unsigned int i=0; i<64; i++) {
		phase++;
		inject_number(256);
		v.clear();
//		setManualPhase(true, phase);
		captureDataIoannis(v, capLength, nChannels);
		printf("%02d\t.", phase);
        //myHistogram->SetBinContent(10, i, 1);
		printDataIoannisPadDecimal(v, true); 
	}
}

void debugMeIoannisStrip(unsigned int capLength, unsigned int nChannels, bool sorted) {
	// printf("Capturing Data on %d streams\n", nChannels);
	std::vector<StreamData> v;
	//TH2F* myHistogram = new TH2F("myName", "againMyName", 128, 0, 127, 66, 0, 65);
	int phase = 0;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisStrip(v, true); 
	inject_number(3);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisStrip(v, true); 
	inject_number(0);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisStrip(v, true); 
	inject_number(0);
	phase++;
	v.clear();
	captureDataIoannis(v, capLength, nChannels);
	printf("%02d\t.", phase);
	printDataIoannisStrip(v, true); 
	for (unsigned int i=0; i<64; i++) {
		phase++;
		inject_zero();
		v.clear();
//		setManualPhase(true, phase);
		captureDataIoannis(v, capLength, nChannels);
		printf("%02d\t.", phase);
        //myHistogram->SetBinContent(10, i, 1);
		printDataIoannisStrip(v, true); 
	}
}
